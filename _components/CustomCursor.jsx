"use client"
import '@/_components/styles/CustomCursor.css'
import { useEffect,useRef } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircle } from '@fortawesome/free-solid-svg-icons'

export default function CustomCursor() {
    const refCursor=useRef(null)
    
    useEffect(()=>{
        const onMouseMove=(event)=>{
            const {scrollX,scrollY}=window;
            const {clientX,clientY}=event;
            const size=refCursor.current.getBoundingClientRect().width
            refCursor.current.style.top=`${clientY-(size/2)+scrollX}px`
            refCursor.current.style.left=`${clientX-(size/2)}px`
        }
        
        const onMouseEnterLink=()=>{
            refCursor.current?.classList?.add('__touch_link')
        }

        const onMouseLeaveLink=()=>{
            refCursor.current?.classList?.remove('__touch_link')
        }

        const links=document.querySelectorAll('a');
        links.forEach(link => {
            link.addEventListener('mouseenter',onMouseEnterLink)
            link.addEventListener('mouseleave',onMouseLeaveLink)
        });

        document.addEventListener('mousemove',onMouseMove)
    },[])
    return <>
        <div ref={refCursor} className='custom-cursor'> <FontAwesomeIcon icon={faCircle}></FontAwesomeIcon> </div>
    </>
}