"use client"
import '@/_components/styles/CometeCursor.css'
import { useEffect,useState } from 'react'

/**
 * 
 * @param {DOMElement} element 
 * @param {Number} x 
 * @param {Number} y 
 * @param {Number} width 
 */
function setPos(element,x,y,width) {
    if (element) {
        element.style.left=`${x-(width/2)}px`;
        element.style.top=`${y-width/2}px`;
    }
}

export default function CometeCursor({black}) {
    const [circles,setCircles]=useState([])
    useEffect(()=>{
        setCircles(document.querySelectorAll('.circle'))
    },[])


    useEffect(()=>{
        if (circles.length!=0) {
            const width=circles[0]?.getBoundingClientRect()?.width
            document.addEventListener('mousemove',function(event) {
                const {clientX,clientY}=event;

                circles.forEach((circle,i) => {
                    setPos(circle,clientX,clientY,width)
                });
            })
            
            const elementsCliquables = document.querySelectorAll('.__clickable, a, select, button, input,*[contentEditable="true"]');
            const handleMouseEnterCliquable = (event) => {
                circles[0].classList.add("__on_mouseEnterCliquable")
            }
            const handleMouseLeaveCliquable = (event) => {
                circles[0].classList.remove("__on_mouseEnterCliquable")
            }
            elementsCliquables.forEach(element => {
                element.addEventListener('mouseenter', handleMouseEnterCliquable);
                element.addEventListener('mouseleave', handleMouseLeaveCliquable);
            });
        }
    },[circles])

   

    return <>
        <div className={(black? "__black_cursor" :"" )+" circle circle1"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle2"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle3"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle4"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle5"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle6"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle7"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle8"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle9"}></div>
        <div className={(black? "__black_cursor" :"" )+" circle circle10"}></div>
    </>
}